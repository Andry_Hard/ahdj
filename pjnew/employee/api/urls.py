from django.conf.urls import url
from rest_framework import routers
from .views import *


router = routers.SimpleRouter()
router.register(r'employees', EmployeeViewSet)

urlpatterns = [
    url(r'search/', SearchEmpl.as_view(), name="search"),
    url(r'departments/$', DepListAPI.as_view(), name="dep-list"),
]

urlpatterns += router.urls
