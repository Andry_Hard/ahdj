from rest_framework import viewsets, generics
from .serializers import EmployeeSerializer, DepSerializer
from employee.models import Employee, Department


__all__ = ("EmployeeViewSet", "SearchEmpl", "DepListAPI")


class DepListAPI(generics.ListAPIView):
    queryset = Department.objects.all()
    serializer_class = DepSerializer


class EmployeeViewSet(viewsets.ModelViewSet):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer



class SearchEmpl(generics.ListAPIView):
    serializer_class = EmployeeSerializer

    def get_queryset(self):
        username = self.request.query_params.get("q", None)
        if username:
            queryset = Employee.objects.filter(name__startswith=username)
            return queryset
        else:
            return None
