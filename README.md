Це проект Django+Angular

Для запуску проекту потрібно:

1) Клонувати даний репозиторій

git clone https://bitbucket.org/Andry_Hard/ahdj.git

cd ahdj

2) необхідно провести комплектацію усіх залежностей і бібліотек:

Версія1 Якщо у Вас є docker !

docker-compose build
docker-compose up 

після того відкриваємо ще один термінал, або якщо docker працює в фоні 
можна в одному терміналі виконуємо наступні команди

docker-compose run web python pjnew/manage.py makemigrations
docker-compose run web python pjnew/manage.py migrate

Тепер у вас в базі є необхідні таблиці для створення користувача і т п

створюємо суперюзера для доступу в адмінку

docker-compose run web python pjnew/manage.py createsuperuser

Версія 2 Якщо немає docker

Необхідно встановити та налаштувати сервер MySQL

Створити в базі користувача вказаного в settings.py з правами суперюзера
Створити базу данних з власником користуваем якого ви попередньо створили

Тепер встановлюємо pipenv

pip install pipenv

Встановлюємо необхідні бібліотеки

pipenv shell
pipenv install

створюємо суперкористувача для доступу в адмінку

заходимо в папу з файлом manage.py

python manage.py createsuperuser

3) Запускаємо проект і створюємо об'єкти Departments

python manage.py runserver
переходимо на урл /admin/ 



4) Користуємося інтерфейсом у звичному режимі

Приємниго користування !


