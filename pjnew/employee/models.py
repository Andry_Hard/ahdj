from django.db import models


__all__ = ("Department", "Employee")


class Department(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name


class Employee(models.Model):
    dp = models.ForeignKey(Department)
    name = models.CharField(max_length=250)
    active = models.BooleanField(default=False)

    def __str__(self):
        return self.name
