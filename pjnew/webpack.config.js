var path = require('path');
var webpack = require('webpack');
const BundleTracker = require('webpack-bundle-tracker');
var UglifyJSPlugin = require('uglifyjs-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
resolveLoader: {
        modules: ['node_modules', '.']
},

    entry:{
        'main': './src/main.ts'
    },
    plugins:[
        new BundleTracker({filename: 'webpack-stats-angular.json'})
    ],
    output: {
        path: __dirname+'/assets/bundles/angular',
        filename: "[name]-[hash].js",
        publicPath: "http://127.0.0.1:8000/static/"//1
    },
       resolve: {
    extensions: ['.ts', '.js']
  },
   module:{
  			},
   };
