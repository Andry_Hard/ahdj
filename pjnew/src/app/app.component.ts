import { Component, OnInit, AfterViewInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { NgxSmartModalService } from 'ngx-smart-modal';

export class Employee {
  constructor(
  public id: number,
  public name: string,
  public isActive: boolean,
  public dep: number,
) {}
}

interface Dep {
  id: number;
  name: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements AfterViewInit{
  base_api = "http://127.0.0.1:8000/api/employees/";
  search_api = "http://127.0.0.1:8000/api/search/";
  dep_api = "http://127.0.0.1:8000/api/departments/";
  title = 'pjnew';
  department: Dep[] = [];
  employees: Employee[] = [];
  result: Employee[] = [];
  employee: Employee ;

  constructor(private http: HttpClient, public NgxSmartModalService: NgxSmartModalService ) {}

  ngAfterViewInit() {
    this.getEmpl();
    this.http.get(this.dep_api).subscribe( data => {
      this.department = data['results'];
    },
    (err) => {
      if (err.error instanceof Error) {
        console.log('Client-side error occured.');
      } else {
        console.log('Server-side error occured.');
      }
    })
  }

  getEmpl() {
    this.http.get(this.base_api).subscribe( data => {
      this.employees = data['results'];
    },
    (err) => {
      if (err.error instanceof Error) {
        console.log('Client-side error occured.');
      } else {
        console.log('Server-side error occured.');
      }
    })
  }

  addEmpl(name: string, isActive: boolean, dep: number){
    this.http.post(this.base_api , {
      "name": name,
      "active": isActive,
      "dp": dep
    }).subscribe(res => {
      console.log(res);
      this.getEmpl();
    },
    (err) => {
      if (err.error instanceof Error) {
        console.log('Client-side error occured.');
      } else {
        console.log('Server-side error occured.');
      }
    }
    )

  }

  toSearch(q: string){
    console.log(q);
    let params = new HttpParams().set("q", q.toString());
    this.http.get(this.search_api, {params}).subscribe( data => {
      this.result = data['results'];
    },
    (err) => {
      if (err.error instanceof Error) {
        console.log('Client-side error occured.');
      } else {
        console.log('Server-side error occured.');
      }
    }
  )
  }

  goToPage(page: number){
    let params = new HttpParams().set("page", page.toString());
    this.http.get(this.base_api, {params}).subscribe( data => {
      this.employees = data['results'];
      console.log(data);
    },
    (err) => {
      if (err.error instanceof Error) {
        console.log('Client-side error occured.');
      } else {
        console.log('Server-side error occured.');
      }
    })

  }

  editEmpl(item: Employee) {
    this.NgxSmartModalService.setModalData(item, 'editModal', true);
    this.NgxSmartModalService.getModal('editModal').open();
    console.log(item);
  }

  deleteEmpl(item: Employee) {
    this.NgxSmartModalService.setModalData(item, 'deleteModal');
    this.NgxSmartModalService.getModal('deleteModal').open();
    console.log(item);
  }

  editEmployeeById(id: number, name: string, active: boolean, dp: number) {
    console.log(id);
    console.log(name);
    console.log(active);
    console.log(dp);
    let upd_url: string = this.base_api + id + "/";
    this.http.put(upd_url, {
      "name": name,
      "active": active,
      "dp": dp,
    }).subscribe(res => {
      console.log(res);
      this.getEmpl();
    },
    (err) => {
        console.log(err);
    })
    }

    deleteEmployeeById(id: number) {
      console.log(id);
      let del_url: string = this.base_api + id + "/";
      this.http.delete(del_url).subscribe(res => {
        console.log(res);
        this.getEmpl();
      },
      (err) => {
          console.log(err);
      })
      }

}
